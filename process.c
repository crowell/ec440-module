#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/mm.h>

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("EC440 Final Project");
MODULE_AUTHOR("Jeffrey Crowell, Samir Ahmed, Richard Tia");

static int pid_mem = 1;

static struct proc_dir_entry *proc_entry;


int process_read( char *page, char **start, off_t off,
                  int count, int *eof, void *data )
{
    int len;
    len = sprintf(page, "%s\n", "check dmesg for the processes");
    struct task_struct* task;
    printk("\nGot the process id to look up as %d.\n", pid_mem);
    for_each_process(task)
    {
//	    len += sprintf(page + strlen(page), "name:[%s] pid:[%d] priority:[%d] state:[%d] \n", task->comm, task->pid, task->rt_priority, task->state);
        printk("name:[%s] pid:[%d] priority:[%d] state:[%d] \n", task->comm, task->pid, task->rt_priority, task->state);
    }
    return len;
}

int init_process_module( void )
{
    int ret = 0;

    proc_entry = create_proc_entry( "process", 0644, NULL );

    if (proc_entry == NULL)
    {
        ret = -ENOMEM;
        printk(KERN_INFO "OH NO!: Couldn't create /proc/process entry\n");
    }
    else
    {
        proc_entry->read_proc = process_read;
        printk(KERN_INFO "process: Module loaded.\n");
    }

    return ret;
}


void cleanup_process_module( void )
{
    remove_proc_entry("process", NULL);
    printk(KERN_INFO "process: Module unloaded, goodbye cruel world!\n");
}


module_init( init_process_module );
module_exit( cleanup_process_module );

